
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	//health and collitions
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	//delay until active
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	//deactivates it
	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	//crates the ship
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	//it is hit
	Ship::Hit(damage);
}